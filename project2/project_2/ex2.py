import pandas as pd
import matplotlib.pyplot as plt
import scipy.cluster.hierarchy as shc
from sklearn.cluster import AgglomerativeClustering


def run(dataset):
    df = pd.read_csv(dataset)
    threshold = 2
    df_values = df.iloc[:, 8:]

    plt.figure(num=1, figsize=(10, 7))
    plt.title("Dendrogram")
    shc.dendrogram(shc.linkage(df_values, method='average', metric='euclidean'))
    plt.axhline(y=threshold, color='r', linestyle='--')

    cluster = AgglomerativeClustering(
        n_clusters=None,
        metric='euclidean',
        linkage='single',
        distance_threshold=threshold
    )
    cluster.fit(df_values)

    # find outliers
    indexes = list()
    labels = [j for j in cluster.labels_]
    for i in range(cluster.n_clusters_):
        if labels.count(i) == 1:
            for index, label in enumerate(labels):
                if label == i:
                    indexes.append(index)

    print(f'outlier_indexes in {dataset}:\n', indexes)

    for i in indexes:
        df = df.drop(i)

    df.to_csv(f'output_{dataset}', encoding='utf-8', index=False)
    plt.show()


if __name__ == '__main__':
    run('female_dataset_v2.csv')
    run('male_dataset_v2.csv')
