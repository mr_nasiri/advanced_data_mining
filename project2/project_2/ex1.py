import pandas as pd

df_female = pd.read_csv('female_data.csv')
df_male = pd.read_csv('male_data.csv')


''' By using axis=0, we can fill in the missing values in each column with the row averages. '''
df_female.iloc[:, 8:] = df_female.iloc[:, 8:].where(df_female.iloc[:, 8:].notna(), df_female.iloc[:, 8:].mean(axis=1), axis=0)
df_male.iloc[:, 8:] = df_male.iloc[:, 8:].where(df_male.iloc[:, 8:].notna(), df_male.iloc[:, 8:].mean(axis=1), axis=0)

df_female.to_csv('female_dataset_v2.csv', encoding='utf-8', index=False)
df_male.to_csv('male_dataset_v2.csv', encoding='utf-8', index=False)