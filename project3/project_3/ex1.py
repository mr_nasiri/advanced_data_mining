import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('dataset.csv')

print('number of attributes:', len(df.columns))
# or print(df.shape[1])

# ----------------------------------------------------------

part_90 = df.sample(frac=0.9)
print('train set size: ', part_90.shape[0])
rest_part_10 = df.drop(part_90.index)
print('test set size: ', rest_part_10.shape[0])

# ----------------------------------------------------------

plt.hist(df['deltaG'])
plt.hist(part_90['deltaG'])
plt.hist(rest_part_10['deltaG'])
plt.show()
